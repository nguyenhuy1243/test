var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var router = express.Router();
var mongoose = require('mongoose');
var userRouters = require('./routers/user.router');
var bookRouter = require('./routers/book.router');
var loginRouter = require('./routers/auth.routers');
var errorHandler = require('./middle-ware/error-handler');
var fileUpload = require('express-fileupload');
var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-access-token');
    next();
};
app.use(allowCrossDomain);
app.use(express.static('public')); // de public cho client co the su dung duoc file trong thu muc do
app.use(fileUpload());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api/user', userRouters);
app.use('/api/book', bookRouter);
app.use('/api/login', loginRouter);


mongoose.connect('mongodb://localhost:27017/Crosstech', (err) => {
    if (err) {
        console.log('not connect to the database');
    } else {
        console.log('Suucessfully connected to MongoDB')
    }
})
app.use(errorHandler.errorHandler());
app.listen(8080, (err) => {
    console.log('server running 8080!!')
});
