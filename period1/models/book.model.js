var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bookSchema = new Schema({
   
    image: {
        type: String,
        default:"unknow"
    },
    title: {
        type: String,
        required:true
    },
    describe:{
        type: String,
        required:true
    },
    date:{
        type:Date,
        required:true
    },
    another: {
        type: String,
        default:"unknow"
    }
})

var book = mongoose.model('book',bookSchema);
module.exports = book;