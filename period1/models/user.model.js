var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email:{
        type: String,
        required:true
    },
    avatar:{
        type:String,
        default:"unkown"
    },
    books: [{
        type: Schema.Types.ObjectId,
        ref: 'book'
    }]

})
var user = mongoose.model('user',userSchema);
module.exports = user;