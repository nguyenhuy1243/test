var router = require('express').Router();
var Book = require('../models/book.model');
var path = require('path');
const uuid = require('uuid');
var bookController = require('../controllers/book.controller');

router.delete('/:id',deleteBook);
router.put('/:id',updateBook);
router.post('/', createBook);
router.get('/', getBook);
router.post('/uploadfile/:id', upfileImage);
router.post('/createBookandImage', createBookandImage);
module.exports = router;
function deleteBook(req,res,next){
    var id = req.params.id;
    bookController.deleteBook(id)
        .then(()=>{
           return res.json({status:201,message:'success'});
        })
        .catch((err)=>{
            return next(err);
        })
}
function updateBook(req,res,next){
    var book = req.body;
    console.log(book+'book')
    var id = req.params.id;
    bookController.updateBook(id,book)
        .then((data)=>{
            return res.json({
                message:'success'
            })
        })
        .catch((err)=>{
            return next(err);
        })
}
function createBookandImage(req, res, next) {
    var data =new Book(JSON.parse(req.body.data));
    var id = req.params.id;
    if (!req.files) {
        return res.status(400).send('No files were uploaded');
    }
    let file = req.files.file;
    console.log(file+'file');
     data.image = 'book_'+ uuid.v4() + '.png';
    file.mv(path.join(__dirname, '../public/image/' + data.image  ), (err) => {
        if (err) {
            return next(err);
        }
   })
   data.save();
   bookController.createBookandImage(data)
   .then((book)=>{
       return res.json({message:'success'});
   })
   .catch((err)=>{
       return next(err);
   })
}
function upfileImage(req, res) {
    var id = req.params.id;
    if (!req.files) {
        return res.status(400).send('No files were uploaded');
    }
    let file = req.files.file;
    bookController.upfileImage(id, file)
        .then((image) => {
            return res.send({
                image: image
            })
        })
        .catch((err) => {
            return res.send('fail!')
        })
}
function getBook(req, res) {
    bookController.getBooks()
        .then((books) => {
            return res.json(books)
        })
        .catch((err) => {
            return res.json({
                message: 'fail!',
                status: '500'
            })
        })
}
function createBook(req, res) {
    var book = new Book(req.body);
    console.log(book);
    bookController.createBook(book)
        .then((book) => {
            return res.json({ message: 'success' });
        })
        .catch((err) => {
            return res.send(err);
        })
} 