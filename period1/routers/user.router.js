

var router = require('express').Router();
var User = require('../models/user.model');
var userController = require('../controllers/user.controller');
var auth = require('../middle-ware/auth');

router.post('/', createUser);
router.get('/', auth.auth(), getUsers);
router.post('/avatar/:id',uploadAvatar)


module.exports = router;
function uploadAvatar(req,res,next){
    var id = req.params.id;
    if(!req.files){
        return res.status(400).send('No files were uploaded');
    }
    let file = req.files.file;
    userController.uploadAvatar(id,file)
        .then((avatar)=>{
            return res.send({
                avatar: avatar
            })
        })
        .catch((err)=>{
            return next(err);
        })
}
function getUsers(req,res,next){
    userController.getUsers()
        .then((users)=>{
            return res.json({
                users: users
            })
        })
        .catch((err)=>{
            return next(err);
        })
}
function createUser(req, res, next) {
  
    var user =new User(req.body);
    console.log(req.body);
    if (!user.email) {
        next({
            message: "Email is required"
        })
    } else if (!user.password) {
        next({
            message: "Password is required"
        })
    } if (!user.name) {
        next({
            message: "Name is required"
        })

    }else{
        userController.createUser(user)
            .then(()=>{
                 res.json({
                        
                     message:'success'
                 });
            })
            .catch((err)=>{
                 res.send(err);
            })
    }
}