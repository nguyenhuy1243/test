
var User = require('../models/user.model');
var path = require('path');
var crypto = require('crypto');
var secret = 'harrypotter';
var mail = require('../utils/mail');
module.exports = {
    createUser: createUser,
    getUsers: getUsers,
    uploadAvatar: uploadAvatar
}
function uploadAvatar(userId, file) {
    return User.findOne({ _id: userId })
        .then((user) => {
            if (user) {
                return new Promise((resolve, reject) => {
                    file.mv(path.join(__dirname, '../public/avatar/user_' + user._id + '.png'), (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return User.update({ _id: userId }, { $set: { avatar: 'user_' + user._id + '.png' } })
                            .then((data) => {
                                return resolve(data);
                            })
                            .catch((err) => {
                                return reject(err);
                            })
                    });
                });
            } else {
                return Promise.reject({
                    message: "up load avatar fail!",
                    statusCode: 404
                })
            }

        })
        .catch((err) => {
            return Promise.reject(err);
        })
}
function getUsers() {
    return User.find()
        .then((users) => {
            return Promise.resolve(users)
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}
function createUser(newUser) {
    return User.find({ email: newUser.email })
        .then(function (foundUsers) {
            if (foundUsers.length > 0) {
                return Promise.reject({
                    statusCode: 400,
                    message: 'Email is existed'
                });
            } else {
                var hash = crypto.createHmac('sha256', secret)
                    .update(newUser.password)
                    .digest('hex');
                newUser.password = hash;
                var user = new User(newUser);
                return user.save()
                    .then(function (user) {
                        console.log( user);
                        return Promise.resolve(user);
                       
                    })
                    .catch(err => {
                        console.log(err);
                        return Promise.reject(err);
                    })
            }
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}