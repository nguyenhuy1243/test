var Book = require('../models/book.model');
var path = require('path');

module.exports = {
    deleteBook:deleteBook,
    updateBook: updateBook,
    createBookandImage: createBookandImage,
    createBook: createBook,
    getBooks: getBooks,
    upfileImage: upfileImage
}
function deleteBook(id){
    return Book.deleteOne({_id:id})
        .then(()=>{
            Promise.resolve();
        })
        .catch((err)=>{
            Promise.reject(err);
        })
}
function updateBook(id, book) {
    console.log(book);
    return Book.findByIdAndUpdate({ _id: id }, book)
        .then((data) => {
            return Promise.resolve(data);
        })
        .catch((err) => {
            return Promise.reject({
                message: 'fail!'
            });
        })
}
function createBookandImage(data) {
    return data.save()
        .then((book) => {
            return Promise.resolve(book);
        })
        .catch((err) => {
            return Promise.reject({
                message: 'fail!'
            })
        })
}
function upfileImage(bookId, file) {
    return Book.findOne({ _id: bookId })
        .then((book) => {   
            if (book) {
                return new Promise((resolve, reject) => {
                    file.mv(path.join(__dirname, '../public/image/book_' + book._id + '.png'), (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return Book.update({ _id: bookId }, { $set: { image: 'book_' + book._id + '.png' } })
                            .then((data) => {
                                return resolve(data);
                            })
                            .catch((err) => {
                                return reject(err);
                            })
                    });
                });
            } else {
                return Promise.reject({
                    message: "not Found",
                    statusCode: 404
                });
            }
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}
function getBooks() {
    return Book.find()
        .then((books) => {
            books.forEach((book) => {
                book.image = 'http://localhost:8080/image/' + book.image;
            })
            return Promise.resolve(books);
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}
function createBook(book) {
    return book.save()
        .then((book) => {
            return Promise.resolve(book);
        })
        .catch((err) => {
            return Promise.reject({
                message: 'fail!'
            })
        })
}